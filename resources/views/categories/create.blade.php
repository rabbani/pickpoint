@extends('admin.master')
@section('main-content')
    <div class="well">
        @if(Session::get('success'))
            <div class="alert alert-success">
                <h4>{{Session::get('success')}}</h4>
            </div>
        @endif
    <h3>Create Category</h3>

    <form action="{{route('category.store')}}" method="post">
        {{csrf_field()}}
        <div class="form-group">
            <label for="name">Category Name</label>
            <input type="text" name="name" class="form-control">
        </div>
        <select name="publication_status" id="" class="form-control">
            <option value="1">Publish</option>
            <option value="0">Un Publish</option>
        </select>
        <br>
        <input type="submit" value="Save" class="btn btn-success">
    </form>

    @endsection
    </div>