@extends('master')
@section('main-content')
    <div class="single-section">
        <div class="container">
            <div class="row">
                <div class="single-post">
                    <div class="col-sm-4">
                        <div class="single-image">
                            <div class="thumb-image">
                                <img src="{{asset('images/'.$product->image)}}" alt="" data-imagezoom="true">
                            </div>
                        </div>
                        <div class="single-details">
                            <h3>{{$product->name}}</h3>
                            <h3 class="price">Price: {{$product->price}} tk</h3>
                            <h4>Size: {{$product->size}}</h4>
                        </div>
                    </div>
                    <div class="col-sm-offset-2 col-sm-6">
                        <h3 class="text-center pick_point">Pick Point</h3>
                        @foreach($sellers as $seller)
                            <div class="col-sm-6">
                                <div class="address">

                                    <p> {{$seller->location->address}}</p>
                                    <p>Name: {{$seller->name}}</p>
                                    <p>Phone:{{$seller->phone}}</p>

                                </div>
                            </div>
                        @endforeach


                    </div><!-- col-sm-6 -->
                </div><!-- single-post -->
            </div><!-- row -->
        </div><!-- container -->
    </div><!-- single-section -->

    <div class="container">
        <div class="row">
            <div class="related-product">
                <div class="related-product-title">
                    <h3>RELATED PRODUCT</h3>
                </div>
                <div class="related-product-list">

                    @foreach($more_products as $more_product)

                        @if($product->id!=$more_product->id)
                            <div class="col-sm-3">
                                <a href="{{url('single/'.$more_product['id'])}}">
                                    <div class="product-image">

                                        <img src="{{asset('images/'.$more_product->image)}}" alt="">
                                        <p class="product-title text-center">{{$more_product['name']}}</p>
                                        <h4 class="text-center price">Price {{$more_product['price']}} tk</h4>
                                    </div><!-- product-image -->
                                </a>
                            </div><!-- col-sm-3 -->
                        @endif
                    @endforeach

                </div><!-- related-product-list -->
            </div><!-- related-product -->
        </div><!-- row -->
    </div><!-- container -->

@endsection