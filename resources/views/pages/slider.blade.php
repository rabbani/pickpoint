<div class="section1">
    <div class="container-fluid">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <?php $sliders = \App\Slider::where('publication_status',1)->get(); $sl = 0?>

                @foreach($sliders as $slider)
                    <?php if ($sl == 0) {
                    $sl = 1; ?>
                    <div class="item active">
                        <img src="{{asset('images/'.$slider->image)}}" alt="" style="width:100%;">
                        <div class="carousel-caption wow zoomIn">
                            <h3>{{$slider->title}}</h3>
                        </div>
                    </div>

                    <?php } else{ ?>
                    <div class="item">
                        <img src="{{asset('images/'.$slider->image)}}" alt="" style="width:100%;">
                        <div class="carousel-caption wow rollIn">
                            <h3>{{$slider->title}}</h3>

                        </div>
                    </div>
                    <?php  } ?>

                @endforeach

            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div><!-- carousel -->
    </div><!-- container-fluid -->
</div><!-- section1 -->