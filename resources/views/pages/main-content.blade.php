@extends('master')
@include('pages.slider')
@section('main-content')
    <div class="section2">
        <div class="container">
            <div class="row">

                @if($catsec1 !=null)
                    <div class="category-title">
                        <p><a href="{{asset('cat/'.$catsec1['id'])}}">{{$catsec1['name']}}</a></p>
                    </div>
                    @foreach($cat_sec_ones as $cat_sec_one)
                        <div class="col-sm-3">
                            <a href="{{url('single/'.$cat_sec_one['id'])}}">
                                <div class="product-image">
                                    <img src="{{asset('images/'.$cat_sec_one['image'])}}" alt="">
                                    <div class="product-image_bottom">
                                        <p class="text-center">{{$cat_sec_one['name']}}</p>
                                        <h4 class="text-center price">Price {{$cat_sec_one['price']}} tk</h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                <div class="col-sm-12">
                    <a href="{{url('cat/'.@$cat_sec_one->category.@$cat_sec_one['category_id'])}}"
                       class="view-all pull-right btn btn-default" type="button">VIEW ALL {{$catsec1['name']}}</a>
                </div>
                    @endif
            </div><!-- row -->
        </div><!-- container -->
    </div><!-- section2 -->

    <div class="section3">
        <div class="container">
            <div class="row">
                @if($catsec2 !=null)
                    <div class="category-title">
                        <p><a href="{{asset('cat/'.@$catsec2['id'])}}">{{@$catsec2['name']}}</a></p>
                    </div>
                    @foreach($cat_sec_twos as $cat_sec_two)

                        <div class="col-sm-3">
                            <a href="{{url('single/'.$cat_sec_two->id)}}">
                                <div class="product-image">
                                    <img src="{{asset('images/'.$cat_sec_two->image)}}" alt="">
                                    <p class="text-center">{{$cat_sec_two->name}}</p>
                                    <h4 class="text-center price">Price {{$cat_sec_two->price}} tk</h4>

                                </div>
                            </a>
                        </div>
                    @endforeach
                    <div class="col-sm-12">
                    <a href="{{url('cat/'.@$cat_sec_two->category.@$cat_sec_two['category_id'])}}"
                       class="view-all pull-right btn btn-default" type="button">VIEW ALL {{$catsec2['name']}}</a>
                    </div>
                @endif
            </div><!-- row -->
        </div><!-- container -->
    </div><!-- section3 -->

    <div class="section4">
        <div class="container">
            <div class="row">
                @if($catsec3 !=null)
                    <div class="category-title">
                        <p><a href="{{asset('cat/'.@$catsec3['id'])}}">{{@$catsec3['name']}}</a></p>
                    </div>
                    @foreach($cat_sec_threes as $cat_sec_three)

                        <div class="col-sm-3">
                            <a href="{{url('single/'.$cat_sec_three['id'])}}">
                                <div class="product-image">
                                    <img src="{{asset('images/'.$cat_sec_three['image'])}}" alt="">
                                    <p class="text-center">{{$cat_sec_three['name']}}</p>
                                    <h4 class="text-center price">Price {{$cat_sec_three['price']}} tk</h4>

                                </div>
                            </a>
                        </div>
                    @endforeach
                    <div class="col-sm-12">
                    <a href="{{url('cat/'.@$cat_sec_three->category.@$cat_sec_three['category_id'])}}"
                       class="view-all pull-right btn btn-default" type="button">VIEW ALL {{$catsec3['name']}}</a>
                    </div>
                        @endif
            </div><!-- row -->
        </div><!-- container -->
    </div><!-- sectoin4 -->

    <div class="section5">
        <div class="container">
            <div class="row">
                @if($catsec4 !=null)
                    <div class="category-title">
                        <p><a href="{{asset('cat/'.@$catsec4['id'])}}">{{@$catsec4['name']}}</a></p>

                    </div>
                    @foreach($cat_sec_fours as $cat_sec_four)

                        <div class="col-sm-3">
                            <a href="{{url('single/'.$cat_sec_four['id'])}}">
                                <div class="product-image">
                                    <img src="{{asset('images/'.$cat_sec_four['image'])}}" alt="">
                                    <p class="text-center">{{$cat_sec_four['name']}}</p>
                                    <h4 class="text-center price">Price {{$cat_sec_four['price']}} tk</h4>

                                </div>
                            </a>
                        </div>
                    @endforeach
                    <div class="col-sm-12">
                    <a href="{{url('cat/'.@$cat_sec_four->category.@$cat_sec_four['category_id'])}}"
                       class="view-all pull-right btn btn-default" type="button">VIEW ALL {{$catsec4['name']}}</a>
                    </div>
                        @endif
            </div><!-- row -->
        </div><!-- container -->
    </div><!-- section5 -->

@endsection