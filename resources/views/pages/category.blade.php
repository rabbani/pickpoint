@extends('master')
@section('main-content')
    <div class="category">
        <div class="container">
            <div class="row">
                <div class="category-title">
                    <p><a href="#">{{@$catname['name']}}</a></p>
                </div>
                @foreach($products as $product)
                <div class="col-sm-3">
                    <a href="{{url('single/'.$product->id)}}">
                        <div class="product-image">
                        <img src="{{asset('images/'.$product->image)}}" alt="">
                        <p class="product-title text-center">{{$product->name}}</p>
                        <h4 class="text-center">Price : {{$product->price}} Tk</h4>
                        </div>
                    </a>
                </div>
                @endforeach
            </div><!-- row -->
        </div><!-- container -->
    </div><!-- category -->
@endsection