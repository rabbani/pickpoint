@extends('admin.master')
@section('main-content')
    @if(Session::get('message'))
        <div class="alert alert-success">
            <h4>{{Session::get('message')}}</h4>
        </div>
    @endif
    @if($errors->all())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors as $error)
                    <li>{{$error->error}}</li>
                @endforeach
            </ul>
        </div>

    @endif
    <div class="well">
        <h3>Create Location</h3>

        <form action="{{route('location.store')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="address">Address</label>
                <textarea name="address" id="address" cols="30" rows="10" class="form-control"></textarea>

            </div>

            <label for="publication_status">Publication Status:</label>
            <select name="publication_status" id="" class="form-control">
                <option value="1">Publish</option>
                <option value="0">Un Publish</option>
            </select>
            <br>
            <input type="submit" value="Save" class="btn btn-success">
        </form>
    </div>
@endsection