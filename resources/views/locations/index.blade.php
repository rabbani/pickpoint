@extends('admin.master')
@section('main-content')

        <div class="well">
            <h3><a href="{{route('location.create')}}">ADD NEW + </a>LOCATION</h3>

            <form action="{{route('location.index')}}" method="get" class="pull-right form-inline">
                <div class="form-group">
                    <input type="text" role="search" class="form-control">
                    <input type="submit" value="search" class="btn btn-info">
                </div>
            </form>

            <table class="table">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>ADDRESS</th>
                    <th>PUBLICATION STATUS</th>
                    <th>ACTION</th>
                </tr>
                </thead>
                <tbody>
                @php($sl=1)
                @foreach($locations as $location)
                    <tr>
                        <td>{{$sl++}}</td>
                        <td>{{$location['address']}}</td>
                        <td>
                            @if($location->publication_status==1)
                                <span class="fa fa-thumbs-o-up text-success">publish</span>
                            @else
                                <span class="fa fa-thumbs-o-down text-danger">un publish</span>
                            @endif

                        </td>
                        <td>
                            @if($location->publication_status==1)
                                <a class="btn btn-success" href="{{asset('unpublished-location/'.$location->id)}}"
                                   style="float: left;margin-right: 3px;">
                                    <i class="glyphicon glyphicon-thumbs-down"></i>
                                </a>
                            @else
                                <a class="btn btn-danger" href="{{asset('published-location/'.$location->id)}}"
                                   style="float: left;margin-right: 3px;">
                                    <i class="glyphicon glyphicon-thumbs-up"></i>
                                </a>
                            @endif
                            {!! Html::decode(Html::linkRoute('location.show','<i class="fa fa-eye">view</i>', [$location->id],['style'=>'margin:5px; float:left'])) !!}
                            {!! Html::decode(Html::linkRoute('location.edit','<i class="fa fa-pencil-square-o">edit</i>', [$location->id],['style'=>'margin:5px; float:left'])) !!}
                            {!! Form::open(['route'=>['location.destroy',$location->id],'method'=>'DELETE']) !!}
                            {{  Form::button( '<i class="fa fa-trash-o"></i>', ['type' => 'submit','style'=>'margin:0;','onclick'=>'return confirm("Are You Sure You Want To Delete This! ")'])}}
                            {!! Form::close() !!}
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>


@endsection