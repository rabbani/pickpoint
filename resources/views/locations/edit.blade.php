@extends('admin.master')
@section('main-content')
    <div class="panel panel-default">

        <div class="panel-heading"><i class="fa fa-laptop"> EDIT LOCATION </i></div>
        @if($errors->all())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors as $error)
                        <li>{{$error->error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(Session::get('message'))
            <div class="alert alert-success">
                <h4>{{Session::get('message')}}</h4>
            </div>
        @endif
        <div class="panel-body">
        {!! Form::model($location,['route'=>['location.update',$location->id],'method' => 'patch']) !!}
        <!-- Title Form Input -->
            <div class="form-group">
                {{Form::label('address','Address:')}}
                {{Form::textarea('address',null,['class'=>'form-control'])}}
            </div>

        <div class="form-group">
            {{ Form::select('publication_status', ['1' => 'Publish', '0' => 'Un Publish'], null, ['placeholder'=>'select status', 'class' => 'form-control'])}}

        </div>

        {{Form::submit('update',['class'=>'btn btn-success btn-sm pull-left','style'=>'margin-right:5px'])}}
        {!! Form::close() !!}
        <a href="{{route('location.index')}}" class="fa fa-arrow-left btn btn-info btn-sm pull-left"> back</a>
        {!! Form::open(['route'=>['location.destroy',$location->id],'method'=>'DELETE']) !!}
        {{  Form::button( '<i class="fa fa-trash-o">delete</i>', ['type' => 'submit','class'=>'btn btn-danger btn-sm btn-delete','style'=>'margin:0 5px;','onclick'=>'return confirm("Are You Sure You Want To Delete This! ")'])}}
        {!! Form::close() !!}
        </div><!-- panel-body -->
    </div><!-- panel panel-default -->

@endsection