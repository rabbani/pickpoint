@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2">
            <nav class="left-menu">
                <ul>
                    <li><a href="{{route('category.index')}}">Category</a></li>
                    <li><a href="">Product</a></li>
                    <li><a href="">Seller</a></li>
                    <li><a href="">Location</a></li>
                </ul>
            </nav>
        </div>
        <div class="col-sm-10">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
