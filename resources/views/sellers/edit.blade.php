@extends('admin.master')
@section('main-content')
    <div class="panel panel-default">

        <div class="panel-heading"><i class="fa fa-laptop"> EDIT SELLER </i></div>
        @if($errors->all())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors as $error)
                        <li>{{$error->error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(Session::get('message'))
            <div class="alert alert-success">
                <h4>{{Session::get('message')}}</h4>
            </div>
        @endif
        <div class="panel-body">
            {!! Form::model($seller,['route'=>['seller.update',$seller->id],'method' => 'patch']) !!}

            <div class="form-group">
                {{Form::label('name','Name:')}}
                {{Form::text('name',null,['class'=>'form-control'])}}
            </div>

            <div class="form-group">
                {{Form::label('phone','Phone:')}}
                {{Form::number('phone',null,['class'=>'form-control'])}}
            </div>
            <div class="form-group">
                <label for="location">Location</label>
                <select id="location"
                        name="location_id"
                        class="form-control">
                    @foreach($locations as $location)
                        @if ($location->id == $seller->location_id)
                            <option selected
                                    value="{{$location->id}}">{{$location->address}}</option>
                        @else
                            <option value="{{$location->id}}">{{$location->address}}</option>
                        @endif
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                {{ Form::select('publication_status', ['1' => 'Publish', '0' => 'Un Publish'], null, ['placeholder'=>'select status', 'class' => 'form-control'])}}

            </div>
            {{Form::submit('update',['class'=>'btn btn-success btn-sm'])}}
            {!! Form::close() !!}
            <a href="{{route('seller.index')}}" class="fa fa-arrow-left btn btn-info btn-sm"> back</a>
            {!! Form::open(['route'=>['seller.destroy',$seller->id],'method'=>'DELETE']) !!}
            {{  Form::button( '<i class="fa fa-trash-o">delete</i>', ['type' => 'submit','class'=>'btn btn-danger btn-sm','style'=>'margin:0;','onclick'=>'return confirm("Are You Sure You Want To Delete This! ")'])}}
            {!! Form::close() !!}

        </div>
    </div>
@endsection