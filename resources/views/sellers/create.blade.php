@extends('admin.master')
@section('main-content')
    <div class="well">
        <h3>Create Seller</h3>
        @if($errors->all())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors as $error)
                        <li>{{$error->error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(Session::get('message'))
            <div class="alert alert-success">
                <h4>{{Session::get('message')}}</h4>
            </div>
        @endif
        <form action="{{route('seller.store')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="name">Seller Name</label>
                <input type="text" name="name" class="form-control">
            </div>
            <div class="form-group">
                <label for="phone">Phone No: </label>
                <input type="number" name="phone" class="form-control">
            </div>
            <div class="form-group">
                <label for="location">Select Location</label>
                <select name="location_id" id="location" class="form-control">

                    @foreach($locations as $location)
                        <option value="{{$location->id}}">{{$location->address}}</option>
                    @endforeach
                </select>
            </div>
            <label for="publication_status">Publication Status:</label>
            <select name="publication_status" id="" class="form-control">
                <option value="1">Publish</option>
                <option value="0">Un Publish</option>
            </select>
            <br>
            <input type="submit" value="Save" class="btn btn-success">
        </form>
    </div>

@endsection