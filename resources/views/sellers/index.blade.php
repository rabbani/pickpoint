@extends('admin.master')
@section('main-content')

        <div class="well">
            <h3><a href="{{route('seller.create')}}">ADD NEW + </a>SELLER</h3>

            <form action="{{route('seller.index')}}" method="get" class="pull-right form-inline">
                <div class="form-group">
                    <input type="text" role="search" class="form-control">
                    <input type="submit" value="search" class="btn btn-info">
                </div>
            </form>

            <table class="table">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>NAME</th>
                    <th>PHONE</th>
                    <th>PUBLICATION STATUS</th>
                    <th>ACTION</th>
                </tr>
                </thead>
                <tbody>
                @php($sl=1)
                @foreach($sellers as $seller)
                    <tr>
                        <td>{{$sl++}}</td>
                        <td>{{$seller['name']}}</td>
                        <td>{{$seller['phone']}}</td>
                        <td>
                            @if($seller->publication_status==1)
                                <span class="fa fa-thumbs-o-up text-success">publish</span>
                            @else
                                <span class="fa fa-thumbs-o-down text-danger">un publish</span>
                            @endif

                        </td>
                        <td>
                            {!! Html::decode(Html::linkRoute('seller.show','<i class="fa fa-eye">view</i>', [$seller->id],['style'=>'margin:5px; float:left'])) !!}
                            {!! Html::decode(Html::linkRoute('seller.edit','<i class="fa fa-pencil-square-o">edit</i>', [$seller->id],['style'=>'margin:5px; float:left'])) !!}
                            {!! Form::open(['route'=>['seller.destroy',$seller->id],'method'=>'DELETE']) !!}
                            {{  Form::button( '<i class="fa fa-trash-o"></i>', ['type' => 'submit','style'=>'margin:0;','onclick'=>'return confirm("Are You Sure You Want To Delete This! ")'])}}
                            {!! Form::close() !!}
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>


@endsection