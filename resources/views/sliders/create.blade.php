@extends('admin.master')
@section('main-content')
    <div class="col-sm-6">
        <div class="well">
            <h3>Create Product</h3>
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>

            @endif
            @if(Session::get('message'))
                <div class="alert alert-success">
                    <h4>{{Session::get('message')}}</h4>
                </div>
            @endif


            {!! Form::open(['route'=>['slider.store'],'files'=>true]) !!}

            <!-- Image Form Input -->
            <div class="form-group">
                {{Form::label('image','Image:')}}
                {{Form::file('image',null,['class'=>'form-control'])}}
            </div>

            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" class="form-control">
            </div>



            <label for="publication_status">Publication Status:</label>
            <select name="publication_status" id="" class="form-control">
                <option value="1">Publish</option>
                <option value="0">Un Publish</option>
            </select>
            <br>
            <input type="submit" value="Save" class="btn btn-success">
            {!! Form::close() !!}
        </div>
    </div>
@endsection