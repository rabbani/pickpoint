@extends('admin.master')
@section('main-content')

        <div class="well">
            <h3><a href="{{route('slider.create')}}">ADD NEW + </a>SLIDER</h3>

            <form action="{{route('slider.index')}}" method="get" class="pull-right form-inline" >
                <div class="form-group">
                    <input type="text" role="search" class="form-control">
                    <input type="submit" value="search" class="btn btn-info">
                </div>
            </form>

            <table class="table">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>IMAGE</th>
                    <th>TITLE</th>
                    <th>PUBLICATION STATUS</th>
                    <th>ACTION</th>
                </tr>
                </thead>
                <tbody>
                @php($sl=1)
                @foreach($sliders as $slider)
                    <tr>
                        <td>{{$sl++}}</td>
                        <td><img src="{{asset('images/'.$slider['image'])}}" alt="" width="100" height="60"></td>
                        <td>{{$slider['title']}}</td>
                        <td>
                            @if($slider->publication_status==1)
                                <span class="fa fa-thumbs-o-up text-success">publish</span>
                            @else
                                <span class="fa fa-thumbs-o-down text-danger">un publish</span>
                            @endif

                        </td>
                        <td>
                            @if($slider->publication_status==1)
                                <a class="btn btn-success" href="{{asset('unpublished-slider/'.$slider->id)}}"
                                   style="float: left;margin-right: 3px;">
                                    <i class="glyphicon glyphicon-thumbs-down"></i>
                                </a>
                            @else
                                <a class="btn btn-danger" href="{{asset('published-slider/'.$slider->id)}}"
                                   style="float: left;margin-right: 3px;">
                                    <i class="glyphicon glyphicon-thumbs-up"></i>
                                </a>
                            @endif
                            {!! Html::decode(Html::linkRoute('slider.show','<i class="fa fa-eye">view</i>', [$slider->id],['style'=>'margin:5px; float:left'])) !!}
                            {!! Html::decode(Html::linkRoute('slider.edit','<i class="fa fa-pencil-square-o">edit</i>', [$slider->id],['style'=>'margin:5px; float:left'])) !!}
                            {!! Form::open(['route'=>['slider.destroy',$slider->id],'method'=>'DELETE']) !!}
                            {{  Form::button( '<i class="fa fa-trash-o"></i>', ['type' => 'submit','style'=>'margin:0;','onclick'=>'return confirm("Are You Sure You Want To Delete This! ")'])}}
                            {!! Form::close() !!}
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
   

@endsection