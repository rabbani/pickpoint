@extends('admin.master')
@section('main-content')
    <div class="panel panel-default">

        <div class="panel-heading"><i class="fa fa-laptop"> EDIT SLIDER </i></div>

        <div class="panel-body">
            {!! Form::model($slider,['route'=>['slider.update',$slider->id],'method' => 'patch']) !!}
            {{--old image--}}
            <div class="form-group">
                <h4>old image</h4>
                {{ Html::image('images/'.$slider->image) }}

            </div>

            <div class="form-group">
                {{Form::label('image','Image:')}}
                {{Form::file('image',null,['class'=>'form-control'])}}
            </div>

            <div class="form-group">
                {{Form::label('title','Title:')}}
                {{Form::text('title',null,['class'=>'form-control'])}}
            </div>

            <!-- Publication_status Form Input -->
            <div class="form-group">
                {{ Form::select('publication_status', ['1' => 'Publish', '0' => 'Un Publish'], null, ['placeholder'=>'select status', 'class' => 'form-control'])}}

            </div>
            {{Form::submit('update',['class'=>'btn btn-success btn-sm'])}}
            {!! Form::close() !!}
            <a href="{{route('slider.index')}}" class="fa fa-arrow-left btn btn-info btn-sm"> back</a>
            {!! Form::open(['route'=>['slider.destroy',$slider->id],'method'=>'DELETE']) !!}
            {{  Form::button( '<i class="fa fa-trash-o">delete</i>', ['type' => 'submit','class'=>'btn btn-danger btn-sm','style'=>'margin:0;','onclick'=>'return confirm("Are You Sure You Want To Delete This! ")'])}}
            {!! Form::close() !!}

        </div>
    </div>
@endsection