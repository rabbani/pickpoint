@extends('admin.master')
@section('main-content')
    <div class="col-sm-6">
        <div class="well">
            <h3>Create Product</h3>
            @if($errors->all())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors as $error)
                            <li>{{$error->error}}</li>
                        @endforeach
                    </ul>
                </div>

            @endif

            @if(Session::get('message'))
                <div class="alert alert-success">
                    <h4>{{Session::get('message')}}</h4>
                </div>
            @endif

            {!! Form::open(['route'=>['product.store'],'files'=>true]) !!}
            <div class="form-group">
                <label for="name">Product Name</label>
                <input type="text" name="name" class="form-control">
            </div>
            <!-- Image Form Input -->
            <div class="form-group">
                {{Form::label('image','Image:')}}
                {{Form::file('image',null,['class'=>'form-control'])}}
            </div>

            <div class="form-group">
                <label for="price">Product Price</label>
                <input type="number" name="price" class="form-control">
            </div>
            <div class="form-group">
                <label for="size">Product Size</label>
                <input type="text" name="size" class="form-control">
            </div>

            <div class="form-group">
                <label for="category">Select Category:</label>
                <select name="category_id" id="category" class="form-control">
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
            </div>

            {{--<div class="form-group">--}}
                {{--<label for="seller">Select Seller :</label>--}}
                {{--<select name="seller_id" id="seller" class="form-control">--}}
                    {{--@foreach($sellers as $seller)--}}
                        {{--<option value="{{$seller->id}}">{{$seller->name}}</option>--}}
                    {{--@endforeach--}}
                {{--</select>--}}
            {{--</div>--}}

            {{--<div class="form-group">--}}
                {{--<label for="location">Select Location :</label>--}}
                {{--<select name="location_id" id="location" class="form-control">--}}
                    {{--@foreach($locations as $location)--}}
                        {{--<option value="{{$location->id}}">{{$location->address}}</option>--}}
                    {{--@endforeach--}}
                {{--</select>--}}
            {{--</div>--}}


            <label for="publication_status">Publication Status:</label>
            <select name="publication_status" id="" class="form-control">
                <option value="1">Publish</option>
                <option value="0">Un Publish</option>
            </select>
            <br>
            <input type="submit" value="Save" class="btn btn-success">
            {!! Form::close() !!}
        </div>
    </div>
@endsection