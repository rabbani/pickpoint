@extends('admin.master')
@section('main-content')

        <div class="well">
            @if($errors->all())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors as $error)
                            <li>{{$error->error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(Session::get('message'))
                <div class="alert alert-success">
                    <h4>{{Session::get('message')}}</h4>
                </div>
            @endif
            <h3><a href="{{route('product.create')}}">ADD NEW + </a>PRODUCT</h3>

            <form action="{{route('product.index')}}" method="get" class="pull-right form-inline" >
                <div class="form-group">
                    <input type="text" role="search" class="form-control">
                    <input type="submit" value="search" class="btn btn-info">
                </div>
            </form>

            <table class="table">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>NAME</th>
                    <th>IMAGE</th>
                    <th>PUBLICATION STATUS</th>
                    <th>ACTION</th>
                </tr>
                </thead>
                <tbody>
                @php($sl=1)
                @foreach($products as $product)
                    <tr>
                        <td>{{$sl++}}</td>
                        <td>{{$product['name']}}</td>
                        <td><img src="{{asset('images/'.$product['image'])}}" alt="" height="50" width="100"></td>
                        <td>
                            @if($product->publication_status==1)
                                <span class="fa fa-thumbs-o-up text-success">publish</span>
                            @else
                                <span class="fa fa-thumbs-o-down text-danger">un publish</span>
                            @endif

                        </td>
                        <td>
                            @if($product->publication_status==1)
                                <a class="btn btn-success" href="{{asset('unpublished-product/'.$product->id)}}"
                                   style="float: left;margin-right: 3px;">
                                    <i class="glyphicon glyphicon-thumbs-down"></i>
                                </a>
                            @else
                                <a class="btn btn-danger" href="{{asset('published-product/'.$product->id)}}"
                                   style="float: left;margin-right: 3px;">
                                    <i class="glyphicon glyphicon-thumbs-up"></i>
                                </a>
                            @endif
                            {!! Html::decode(Html::linkRoute('product.show','<i class="fa fa-eye">view</i>', [$product->id],['style'=>'margin:5px; float:left'])) !!}
                            {!! Html::decode(Html::linkRoute('product.edit','<i class="fa fa-pencil-square-o">edit</i>', [$product->id],['style'=>'margin:5px; float:left'])) !!}
                            {!! Form::open(['route'=>['product.destroy',$product->id],'method'=>'DELETE']) !!}
                            {{  Form::button( '<i class="fa fa-trash-o"></i>', ['type' => 'submit','style'=>'margin:0;','onclick'=>'return confirm("Are You Sure You Want To Delete This! ")'])}}
                            {!! Form::close() !!}
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>


@endsection