<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
//    public function products()
//    {
//        return $this->belongsToMany(Product::class);
//    }
    public function location()
    {
        return $this->belongsTo('App\Location');
    }
}
