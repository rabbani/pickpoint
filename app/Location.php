<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    public function seller()
    {
        return $this->hasOne('App\Seller');
    }
}
