<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=['name','image','price','size','category',];
    public function categories()
    {
        return $this->belongsTo(Category::class);
    }


}
