<?php

namespace App\Http\Controllers;

use App\Seller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Location;

class SellersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $sellers=Seller::all();
       return view('sellers.index')->withSellers($sellers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locations = Location::all();
       return view('sellers.create')->withLocations($locations);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'phone'=>'required',
            'location_id'=>'required',
            'publication_status'=>'required'

        ]);

        $seller=new Seller();
        $seller->name=$request->name;
        $seller->phone=$request->phone;
        $seller->location_id=$request->location_id;
        $seller->publication_status=$request->publication_status;
        $seller->save();

        Session::flash('message','Seller Insert Successfully');
        return redirect()->route('seller.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public  function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seller=Seller::findOrFail($id);
        $locations=Location::all();
        return view('sellers.edit')->withSeller($seller)->withLocations($locations);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required',
            'phone'=>'required',
            'location_id'=>'required',
            'publication_status'=>'required'
        ]);

        $seller=Seller::findOrFail($id);
        $seller->name=$request->name;
        $seller->phone=$request->phone;
        $seller->location_id=$request->location_id;
        $seller->publication_status=$request->publication_status;
        $seller->update();

        Session::flash('message','Seller Update Successfully');
        return redirect()->route('seller.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $seller=Seller::findOrFail($id);
        $seller->delete();
        Session::flash('message','Seller Deleted Successfully');
        return redirect()->route('seller.index');
    }
}
