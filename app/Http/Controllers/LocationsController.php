<?php

namespace App\Http\Controllers;

use App\Location;
use App\Seller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LocationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public  function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $locations = Location::all();
        return view('locations.index')->withLocations($locations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sellers = Seller::all();
        return view('locations.create')->withSellers($sellers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'address' => 'required',
            'publication_status' => 'required'

        ]);

        $location = new Location();
        $location->address = $request->address;
        $location->publication_status = $request->publication_status;
        $location->save();
        Session::flash('message', 'Location Insert Successfully');
        return redirect()->route('location.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $location = Location::findOrFail($id);
        return view('locations.edit')
            ->withLocation($location);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'address' => 'required',
            'publication_status' => 'required',

        ]);

        $location = Location::findOrFail($id);
        $location->address = $request->address;
        $location->publication_status = $request->publication_status;
        $location->update();

        Session::flash('message', 'Location Update Successfully');
        return redirect()->route('location.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $location = Location::findOrFail($id);
        $location->delete();
        Session::flash('message', 'Location Delete Successfully');
        return redirect()->route('location.index');
    }


    public function publish_location($id)
    {
        $location=Location::where('id',$id)->update(['publication_status'=>1]);
        return redirect()->route('location.index')->withSlider($location);
    }

    public function unpublish_location($id)
    {
        $location=Location::where('id',$id)->update(['publication_status'=>0]);
        return redirect()->route('location.index')->withSlider($location);
    }
}
