<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */
    public  function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $categories = Category::all();
        return view('categories.index')->withCategories($categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
           'name'=>'required | min:2 | max:20',
            'publication_status'=>'required'
        ]);

        $category = new Category();
        $category->name=$request->name;
        $category->publication_status=$request->publication_status;
        $category->save();

        return redirect()->route('category.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category= Category::findOrFail($id);
        return view('categories.edit')->withCategory($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required | min:2 | max:20',
            'publication_status'=>'required'
        ]);

        $category= Category::findOrFail($id);
        $category->name=$request->name;
        $category->publication_status=$request->publication_status;
        $category->update();

        Session::flash('message','Data update Successfull');

        return redirect()->route('category.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function publish_category($id)
    {
        $category=Category::where('id',$id)->update(['publication_status'=>1]);
        return redirect()->route('category.index')->withCategory($category);
    }

    public function unpublish_category($id)
    {
        $category=Category::where('id',$id)->update(['publication_status'=>0]);
        return redirect()->route('category.index')->withCategory($category);
    }



    public function destroy($id)
    {
        $category= Category::findOrFail($id);
        $category->delete();
        return redirect()->route('category.index');
    }
}
