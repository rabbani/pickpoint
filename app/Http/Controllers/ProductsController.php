<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Image;
use Storage;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public  function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $products=Product::orderBy('id','desc')->get();
        return view('products.index')->withProducts($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::all();

        return view('products.create')->withCategories($categories);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required | max:255| min:2',
            'image'=>'required',
            'price'=>'required',
            'size'=>'required',
            'category_id'=>'required',
            'publication_status'=>'required',

        ] );

        $product=new Product();
        $product->name=$request->name;
        if($request->hasFile('image')){

            $image=$request->file('image');
            $file_name= time().'.'.$image->getClientOriginalExtension();
            $location=public_path('images/'.$file_name);
            Image::make($image)->fit(600)->save($location);
            $product->image=$file_name;
        }
        $product->price=$request->price;
        $product->size=$request->size;
        $product->category_id=$request->category_id;
        $product->publication_status=$request->publication_status;
        $product->save();
        Session::flash('message','Product Insert Successfully');
        return redirect()->route('product.create');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product= Product::findOrFail($id);
        return view('products.edit')->withProduct($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required | max:255| min:2',
            'image'=>'sometimes',
            'price'=>'required',
            'size'=>'required',
            'category_id'=>'required',
            'publication_status'=>'required',



        ] );

        $product=Product::findOrFail($id);
        $product->name=$request->name;
        if($request->hasFile('image')){

            $image=$request->file('image');
            $file_name= time().'.'.$image->getClientOriginalExtension();
            $location=public_path('images/'.$file_name);
            Image::make($image)->fit(600)->save($location);
            $product->image=$file_name;
        }
        $product->price=$request->price;
        $product->size=$request->size;
        $product->category_id=$request->category_id;
        $product->publication_status=$request->publication_status;
        $product->update();
        Session::flash('message','Product Update Successfully');
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product=Product::findOrFail($id);
        Storage::delete($product->image);
        $product->delete();
        Session::flash('message','Product Delete Successfully');
        return redirect()->route('product.index');
    }


    public function publish_product($id)
    {
        $product=Product::where('id',$id)->update(['publication_status'=>1]);
        return redirect()->route('product.index')->withProduct($product);
    }

    public function unpublish_product($id)
    {
        $product=Product::where('id',$id)->update(['publication_status'=>0]);
        return redirect()->route('product.index')->withProduct($product);
    }
}
