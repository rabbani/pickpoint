<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Image;
use Storage;
class SlidersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders=Slider::all();
        return view('sliders.index')->withSliders($sliders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sliders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request, [
          'image'=>'required',
          'title'=>'required',
          'publication_status'=>'required'
       ]);

       $slider= new Slider();
        if($request->hasFile('image')){

            $image=$request->file('image');
            $file_name= time().'.'.$image->getClientOriginalExtension();
            $location=public_path('images/'.$file_name);
            Image::make($image)->resize(800,300)->save($location);
            $slider->image=$file_name;
        }
        $slider->title=$request->title;
        $slider->publication_status=$request->publication_status;
        $slider->save();
        Session::flash('message','Slider Insert Successfully');
        return redirect()->route('slider.create');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider= Slider::findOrFail($id);
        return view('sliders.edit')->withSlider($slider);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'image'=>'required',
            'title'=>'required',
            'publication_status'=>'required'
        ]);

        $slider= Slider::findOrFail($id);
        if($request->hasFile('image')){

            $image=$request->file('image');
            $file_name= time().'.'.$image->getClientOriginalExtension();
            $location=public_path('images/'.$file_name);
            Image::make($image)->resize(800,300)->save($location);
            $slider->image=$file_name;
        }
        $slider->title=$request->slider;
        $slider->publication_status=$request->publication_status;
        $slider->update();
        Session::flash('message','Slider Update Successfully');
        return redirect()->route('slider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider=Slider::findOrFail($id);
        Storage::delete($slider->image);
        $slider->delete();
        Session::flash('message','Slider Delete Successfully');
        return redirect()->route('slider.index');
    }

    public function publish_slider($id)
    {
        $slider=Slider::where('id',$id)->update(['publication_status'=>1]);
        return redirect()->route('slider.index')->withSlider($slider);
    }

    public function unpublish_slider($id)
    {
        $slider=Slider::where('id',$id)->update(['publication_status'=>0]);
        return redirect()->route('slider.index')->withSlider($slider);
    }

}
