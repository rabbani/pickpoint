<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Slider;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $catsec1 = Category::where('cat_section', 1)
            ->where('publication_status', 1)
            ->first();
        $catsec2 = Category::where('cat_section', 2)
            ->where('publication_status', 1)
            ->first();
        $catsec3 = Category::where('cat_section', 3)
            ->where('publication_status', 1)
            ->first();
        $catsec4 = Category::where('cat_section', 4)
            ->where('publication_status', 1)
            ->first();
//        $catsec_one_post = Post::where('category_id', $catsec1['id'])->where('publication_status', 1)->where('cat_in_position', '>', 0)->orderBy('id','desc')->take(4)->get();
        $cat_sec_ones=Product::where('category_id', $catsec1['id'])->where('publication_status', 1)->orderBy('id','desc')->take(8)->get();
        $cat_sec_twos=Product::where('category_id',$catsec2['id'])->where('publication_status', 1)->orderBy('id','desc')->take(8)->get();
        $cat_sec_threes=Product::where('category_id',$catsec3['id'])->where('publication_status', 1)->orderBy('id','desc')->take(8)->get();
        $cat_sec_fours=Product::where('category_id',$catsec4['id'])->where('publication_status', 1)->orderBy('id','desc')->take(8)->get();
//dd($catsec1);
        return view('pages.main-content')
            ->with('catsec1', $catsec1)
            ->withCat_sec_ones($cat_sec_ones)
            ->with('catsec2', $catsec2)
            ->withCat_sec_twos($cat_sec_twos)
            ->with('catsec3', $catsec3)
            ->withCat_sec_threes($cat_sec_threes)
            ->with('catsec4', $catsec4)
            ->withCat_sec_fours($cat_sec_fours);
//            ->withCategory($category);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
