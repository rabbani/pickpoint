<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/','WelcomeController@index');
Route::get('/single/{id}','SingleController@show');
Route::get('/cat/{id}','PagesController@show');

//Route::get('/single', function () {
//    return view('pages.single');
//});

Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');

// manage category
Route::get('published-category/{id}','CategoriesController@publish_category');
Route::get('unpublished-category/{id}','CategoriesController@unpublish_category');
Route::resource('category','CategoriesController');

// manage slider
Route::get('published-slider/{id}','SlidersController@publish_slider');
Route::get('unpublished-slider/{id}','SlidersController@unpublish_slider');
Route::resource('slider','SlidersController');

// manage category
Route::get('published-product/{id}','ProductsController@publish_product');
Route::get('unpublished-product/{id}','ProductsController@unpublish_product');
Route::resource('product','ProductsController');
// manage location
Route::get('published-location/{id}','LocationsController@publish_location');
Route::get('unpublished-location/{id}','LocationsController@unpublish_location');
Route::resource('location','LocationsController');
Route::resource('seller','SellersController');

